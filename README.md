# Manda Trampo

## From Zero to App - Part I

### Crie um novo projeto Flutter
1. Com o Visual Studio Code aberto, pressione a combinação `CTRL + SHIFT + P`.
![](images/novo_projeto.png)

<img src="images/tela_projeto_inicial.png" height="480">

### Preparar a estrutura de tela inicial
1. Após a IDE configurar as dependias e a estrutura inicial do projeto; remova o conteúdo dos arquivos `lib/mais.dart` e `test/widget_test.dart`.
2. Configure o lançamento da tela inicial do App. Criaremos um Stateless Widget contendo um MaterialApp que irá conter o elemento raiz de nossos Widgets e o tema utilizado no MandaTrampo. Adicione o trecho abaixo em `main.dart`.

```javascript
import 'package:flutter/material.dart';

main() => runApp(MandaTrampo());

class MandaTrampo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        //Configurações do Tema
      ),
      home: LoginPage(),
    );
  }
}
```

### Configurar o tema do App
Qualquer semelhança com o conceito de CSS, é mera coincidência...
O atributo theme armazena as informações de tema dos componentes, se quisermos padronizar os componentes e garantir o menor esforço o mais conveniente seria utilizar um objeto ThemeData.
Vamos adicionar as cores iniciais do nosso mockup com um objeto ThemeData.


1. Adicione o trecho abaixo dentro do objeto ThemeData()

```javascript
theme: ThemeData(
    //Altera as cores dos componentes para o Tema Escuro
    brightness: Brightness.dark,
    accentColor: Color(0xFF746EFF),
    // Cor de fundo do Scaffold
    scaffoldBackgroundColor: Color(0xFF171717),
    // Configuração de estilo do texto por categoria
    textTheme: TextTheme(
      bodyText2: TextStyle(color: Color(0xFF7D7B85)),
    ),
    // Cor padrão dos botões
    buttonTheme: ButtonThemeData(
      buttonColor: Color(0xFF746EFF),
    ),
    // Decoração das caixas de texto com bordas arredondadas
    inputDecorationTheme: InputDecorationTheme(
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Color(0xFF7D7B85),
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Color(0xFF7D7B85),
        ),
        borderRadius: BorderRadius.circular(10),
      ),
    ),
  ),
```

### Adicionar a tela de login já utilizando o tema
1. Crie um novo diretório dentro da pasta `lib`, o diretório `lib/screens`.
2. Dentro de `lib/screens` crie o arquivo `login_page.dart`.
3. Iremos criar uma tela com scroll para que possamos rolar os componentes do formulário. Adicione o trecho abaixo no arquivo `login_page.dart`.

```javascript
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                // Texto e coração em mesma linha
               Row(
                 children: [
                   Text(
                     'VAIPASSAR ',
                     style: TextStyle(
                         color: Theme.of(context).accentColor,
                         fontWeight: FontWeight.w500),
                   ),
                   Icon(
                     Icons.favorite,
                     color: Theme.of(context).accentColor,
                     size: 18,
                   )
                 ],
               ),
               // Imagem com logo do mockup
               Image.asset('images/logo.png'),
               SizedBox(
                 height: 16,
               ),
               // Texto com múltiplas formatações utilizando Span, cada RichText possui um texto e um conjunto de TextSpans permitindo múltiplas formatações
               RichText(
                 text: TextSpan(
                   text: 'Queremos te',
                   style: TextStyle(fontSize: 20),
                   children: [
                     TextSpan(
                       text: ' ajudar ',
                       style: TextStyle(color: Color(0xFF6c63fe)),
                       children: [
                         TextSpan(
                           text: ' a passar por tudo isso!',
                           style: TextStyle(color: Colors.white),
                         ),
                       ],
                     ),
                   ],
                 ),
               ),
               // Componente customizado conforme mockup
              ],
            ),
          ),
        ),
      ),
    );
  }
}

```
Completamos a primeira parte do mockup, agora vamos criar o Widget customizado de caixa de texto.

### Criar Widget reaproveitável para input de Texto
1. Crie a pasta `lib/widgets`.
2. Crie o arquivo `text_field_manda_trampo.dart` dentro da pasta widgets.
3. Com base na atividade de criação do formulário de currículos, vamos criar um TextField customizado, mas desta vez estamos aproveitando a configuração do Tema. Adicione o código abaixo no arquivo `text_field_manda_trampo.dart`.

```javascript

import 'package:flutter/material.dart';

class TextFieldMandaTrampo extends StatelessWidget {
  final String texto;
  final TextInputType tipoTeclado;
  final bool isPassword;
  final TextEditingController controller;

  const TextFieldMandaTrampo({this.texto, this.tipoTeclado, this.isPassword, this.controller});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: TextField(
        controller: controller,
        obscureText: isPassword ?? false,
        keyboardType: tipoTeclado ?? TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: texto,
          hintStyle: TextStyle(
            color: Theme.of(context).textTheme.bodyText2.color,
          ),
        ),
      ),
    );
  }
}
```

4. Com nosso Widget pronto, podemos começar a reaproveitá-lo. Logo após o comentário `Componente customizado conforme mockup` no passo 3, adicione a chamada aos widgets criados agora criando as entradas de texto. Adicione o trecho abaixo.

```javascript
// Componente customizado conforme mockup
    TextFieldMandaTrampo(texto: 'Nome completo'),
    TextFieldMandaTrampo(
      texto: 'E-mail',
      tipoTeclado: TextInputType.emailAddress,
    ),
    TextFieldMandaTrampo(
      texto: 'Senha',
      isPassword: true,
    ),
    TextFieldMandaTrampo(
      texto: 'Confirme a senha',
      isPassword: true,
    ),
    // Espaço vertical
    SizedBox(
      height: 32,
    ),
```
5. Vamos adicionar uma linha com um checkbox com um texto de exemplo logo ao lado. Adicione este trecho logo abaixo do passo anterior.

```javascript
Row(
  children: [
    Checkbox(
      onChanged: (bool value) {},
      value: false,
    ),
    Expanded(
      child: Text('Eu concordo com os termos de serviço da mandatrampo.'),
    ),
  ],
),
```

6. Agora adicionaremos uma nova linha com os botões para a navegação inicial.

```javascript
  Row(
    children: [
      Expanded(
        child: MaterialButton(
          child: Text('VOLTAR'),
          onPressed: () {},
          shape: Border.all(
              width: 1, color: Theme.of(context).accentColor),
        ),
      ),
      SizedBox(
        width: 32,
      ),
      Expanded(
        child: RaisedButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => MenuCadastro()));
          },
          child: Text('PRÓXIMO'),
        ),
      ),
    ],
  ),
  //Espacamento vertical
  SizedBox(
    height: 24,
  ),
```
7. Adicionaremos agora a última parte da tela do mock de login, a linha contendo um botão de verificação de conta já exitente.

```javascript
Row(
  children: [
    Text('Já tem uma conta? '),
    Align(
      alignment: Alignment.centerLeft,
      child: FlatButton(
        onPressed: () {},
        child: Text(
          'Entrar',
          style: TextStyle(color: Colors.blueAccent),
        ),
      ),
    )
  ],
),
```

## Criar tela de seleção de Cadastro
Nesta tela, ao tocarmos no botão próximo da tela de Login, faremos a navegação para a tela de seleção de cadastro.
Nós iremos ajustar o fluxo de navegação conforme formos criando as telas.

1. Crie o arquivo `screens\menu_cadastro_page.dart`.
2. Adicione a estrutura básica da tela.

```javascript
import 'package:flutter/material.dart';

class MenuCadastro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        //Alinhamento
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          //Widgets adicionais aqui...
          ],
        ),
      ),
    );
  }
}
```
3. Adicione o widgets, completando a tela de menu. Logo abaixo do comentário `Widgets adicionais aqui...`, inclua.

```javascript
    //Texto simples
    Text(
      'ESCOLHA UMA DA OPÇÕES',
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 24),
    ),
    //Texto com configurações de margem dentro e um container
    Container(
      margin: EdgeInsets.only(left: 48, right: 48, top: 16),
      child: Text(
        'Não se preocupe, você poderá cadastrar os dois depois =)',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 14),
      ),
    ),
    SizedBox(
      height: 64,
    ),
    RaisedButton(
      //Configuração para cantos arredondados
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(32)),
      child: Text('CADASTRAR CV'),
      onPressed: () {
        // Navegação a ser adicionada
      },
    ),
    SizedBox(
      height: 24,
    ),
    RaisedButton(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(32)),
      child: Text('CADASTRAR SERVIÇO/NEGÓCIO'),
      onPressed: () {
        // Navegação a ser adicionada
      },
    ),
```

## Criar uma tela utilizando o componente `BottomNavigationBar` para simular uma possível estrutura de tela principal para o MandaTrampo.

Para a compreensão do componente e a navegação, vamos criar primeiramente uma versão simplificada e depois adiciona um componente que manterá os estados das telas de navegação, mas antes, vamos criar a estrutura básica.

1. Crie o arquivo `screens\homepage.dart`, como de praxe.
2. Adicione o código abaixo.

```javascript
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  var paginaSelecionada = 0;
  var paginas = ['Serviços', 'CV','Configurações'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manda Trampo'),
      ),
      body: Center('MandaTrampo'),
      //BottomNavigationBar aqui...
    );
  }
}
```

O BottomNavigationBar é um componente do do Scaffold, então assim como appBar, body, floatinActionButtom, ele é um atributo; então vamos adicioná-lo logo abaixo do **body**.

3. Logo abaixo do comentário, adicione o trecho do BottomNavigationBar.

```javascript
bottomNavigationBar: BottomNavigationBar(
  currentIndex: paginaSelecionada,
  items: [
    BottomNavigationBarItem(
      icon: Icon(Icons.home),
      title: Text('Serviços'),
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.person),
      title: Text(
        'CV',
      ),
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.settings),
      title: Text(
        'Configurações',
      ),
    ),
  ],
  onTap: (index) {
    setState(() {
      paginaSelecionada = index;
    });
  },
),
```

O BottomNavigationBar é composto por uma barra com ícones, com um limite máximo de 5 itens.
Ele já possui os atributos necessários para  adição de eventos aos ícones e facilitação para navegação entre páginas.

4. Testar os eventos de click no BottomNavigationBar.
No atributo onTap, configuramos a mudança da variável paginaSelecionada para que ela receba o índice do array de botões.
Vamos fazer com que o texto no no body seja alterado conforme a seleção dos itens no BottomNavigationBar.

Criar o array paginas
```javascript
    var paginaSelecionada = 0;
    //Adicionar neste ponto
   var paginas = ['Serviços', 'CV', 'Configurações'];
```

Alterar o **body** no Scaffold
```javascript
body: Center(child: Text(paginas[paginaSelecionada])),
```
### Criar as outras páginas para Navegação.
Ao invés de trocarmos o texto de um widget, vamos navegar entre páginas do App. Vamos criar mais duas páginas simples para realizar a navegação e então adicionar os conteúdos já criados.

#### Criar a página Configuracoes;
1. Crie o arquivo `screens\configuracoes.dart`;
2. Adicione o código abaixo.

```javascript
import 'package:flutter/material.dart';

class Configuracoes extends StatefulWidget {
  @override
  _ConfiguracoesState createState() => _ConfiguracoesState();
}

class _ConfiguracoesState extends State<Configuracoes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
    );
  }
}
```
#### Criar a página Curriculos;
1. Crie o arquivo `screens\curriculos.dart`;
2. Adicione o código abaixo.

```javascript
import 'package:flutter/material.dart';

class Curriculos extends StatefulWidget {
  @override
  _CurriculosState createState() => _CurriculosState();
}

class _CurriculosState extends State<Curriculos> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepOrange,
    );
  }
}
```

#### Criar a página Serviços;
1. Crie o arquivo `screens\servicos.dart`;
2. Adicione o código abaixo.

```javascript
import 'package:flutter/material.dart';

class Servicos extends StatefulWidget {
  @override
  _ServicosState createState() => _ServicosState();
}

class _ServicosState extends State<Servicos> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.pinkAccent,
    );
  }
}
```

### Ajustando a página principal
Agora precisaremos importar os arquivos das telas criadas e configurar a navegação entre as telas.

1. Importe os arquivos; no topo do arquivo `home_page.dart` adicione:

```javascript
import 'package:f290_manda_trampo/screens/configuracoes.dart';
import 'package:f290_manda_trampo/screens/curriculos.dart';
import 'package:tutorial/screens/servicos.dart';
```

2. Crie uma lista de widgets substituindo o array de Strings:

```javascript
  var _paginas = List<Widget>();
```

3. Sobrescreva o método initState(), inicializando a lista de Widgets:

```javascript
@override
void initState() {
  _paginas.add(Servicos());
  _paginas.add(Curriculos());
  _paginas.add(Configuracoes());
  super.initState();
}
```

4. Altere o **body**  com o Widget `IndexedStack`, este tem a capacidade de manter os estados dos objetos de forma transparente, será muito u´til para nossos formulários.

```javascript
body: IndexedStack(
  index: paginaSelecionada,
  children: _paginas,
),
```

5. O código completa para podermos conferir.

```javascript
import 'package:f290_manda_trampo/screens/configuracoes.dart';
import 'package:f290_manda_trampo/screens/curriculos.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  var _paginas = List<Widget>();

  var paginaSelecionada = 0;

  @override
  void initState() {
    _paginas.add(Servicos());
    _paginas.add(Curriculos());
    _paginas.add(Configuracoes());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manda Trampo'),
      ),
      body: IndexedStack(
        index: paginaSelecionada,
        children: _paginas,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: paginaSelecionada,
        items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Serviços'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          title: Text(
            'CV',
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings),
          title: Text(
            'Configurações',
          ),
        ),
      ],
      onTap: (index){
        setState(() {
          paginaSelecionada = index;
        });
      },),
    );
  }
}
```

### Ajustar a página de Serviços

A página de serviços será onde nós iremos visualizar uma lista de Cards contendo uma imagem e alguns dados dos Parceiros do Manda Trampo.
Vamos conhecer e criar o Widget ListView para exibir os Cards de nossos Parceiros. Mas primeiro precisamos de uma fonte de dados para preenchermos nossa lista.
Esta lista será mantida por um arquivo armazenado na memória do aparelho no formato JSON.

1. Adicione a dependência `path_provider`e `font_awesome`. Abra o arquivo `pubspec.yaml` e adicione o trecho:

```javascript
cupertino_icons: ^0.1.3
//Abaixo de cupertino icons
font_awesome_flutter: ^8.8.1
path_provider: ^1.6.10
```
O Path Provider é responsável por localizar os diretórios e arquivos no Android e iOS de forma simples.

2. Vamos criar um arquivo utilitário para gerenciar nosso arquivo de persistencia. Crie o arquivo `utils\file_utils.dart`.
3. Adicione o código ao `FileUtils`.

```javascript
import 'dart:convert' as json;
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:tutorial_manda_trampo/models/parceiros_data.dart';

// Obter caminho do arquivo de persistencia
class FileUtils {
  static Future<File> getArquivo() async {
    final diretorio = await getApplicationDocumentsDirectory();
    return File("${diretorio.path}/arquivo.json");
  }

// Recupera o arquivo, lê e o converte em String em memória em JSON
  static Future<String> lerArquivo() async {
    try {
      final arquivo = await getArquivo();
      return arquivo.readAsString();
    } catch (e) {
      print('Erro ao ler arquivo: ${e.toString()}');
      return null;
    }
  }

// Grava o arquivo no formato JSON
  static Future<File> salvarArquivo() async {
    final dados = json.jsonEncode(ParceiroData.listaParceiros);
    final arquivo = await getArquivo();
    return arquivo.writeAsString(dados);
  }
}

```
 ### Adaptar a lista de dados
 Vamos adaptar nossa tela de serviços para exibir uma coleção de dados de Parceiros.
 Para tal, precisamos criar uma classe model e um widget para apresentar os dados deste model e depois ajustar nossa Tela de Serviços.

1. Crie o arquivo `models\parceiro.dart`.
2. Adicione o código abaixo.

```javascript
class Parceiro {
  String parceiro;
  String endereco;
  String cidade;
  String telefone;
  String url;
  String horario;
  String whatsApp;
  String instagram;
  String imagem;

  //Construtor com parametros opcionais
  Parceiro({this.parceiro, this.imagem, this.endereco, this.cidade,
  this.telefone, this.url, this.horario, this.whatsApp, this.instagram});

  // Metodo para converter o objeto em Mapa para a conversão em JSON
  Map<String, dynamic> toMap() {
    var map = {
      'parceiro': parceiro,
      'endereco': endereco,
      'cidade': cidade,
      'telefone': telefone,
      'url': url,
      'horario': horario,
      'whatsApp': whatsApp,
      'instagram': instagram,
      'imagem': imagem
    };
    return map;
  }
}

```

3. Crie o arquivo `models\parceiros_data.dart`.
4. Adicione o código abaixo.

```javascript
import 'package:tutorial_manda_trampo/models/parceiro.dart';

class ParceiroData {
  static var listaParceiros = [
    Parceiro(
      parceiro: 'Nostra Pizza',
      imagem: 'pizza.jpg',
      endereco: 'Rua Araraquara, 500 - Jd S Joao - Araras/SP',
      telefone: '19983330406',
      url: 'https://www.facebook.com/nostrapizza',
      whatsApp: '19983339876',
      instagram: '@nostrapizza',
      horario: 'Seg-Dom: 06:00 as 21:00',
    ).toMap(),
    Parceiro(
      parceiro: 'Scudetto',
      imagem: 'pizza.jpg',
      endereco: 'Rua Tiradentes, 200 - Centro - Araras/SP',
      telefone: '19983330406',
      url: 'https://www.facebook.com/scudetto',
      whatsApp: '19983332345',
      instagram: '@scudetto',
      horario: 'Seg-Dom: 06:00 as 21:00',
    ).toMap(),
    Parceiro(
      parceiro: 'VegPizza',
      imagem: 'pizza.jpg',
      endereco: 'Rua Salamandra, 400 - Jd Green Ville - Limeira/SP',
      telefone: '19983338877',
      url: 'https://www.facebook.com/vegpizza',
      whatsApp: '19983336539',
      instagram: '@vegpizza',
      horario: 'Seg-Dom: 06:00 as 21:00',
    ).toMap(),
  ];
}

```
Esta lista iremos utilizar apenas na primeira vez, até implementarmos o formulário para inserção de dados.

#### Inserir a ListView na página de Serviços

Atualize o código de `servicos.dart`.

```javascript
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:tutorial_manda_trampo/utils/file_utils.dart';
import 'package:tutorial_manda_trampo/widgets/parceiro_card.dart';

class Servicos extends StatefulWidget {
  @override
  _ServicosState createState() => _ServicosState();
}

class _ServicosState extends State<Servicos> {
  var listaParceiros = [];

/* Este método é chamado sempre que a tela for recriada, então os dados serão carregados
    e exibidos na lista...
*/
  @override
  void initState() {
    FileUtils.lerArquivo().then((value) {
      setState(() {
        listaParceiros = jsonDecode(value);
      });
    });
    super.initState();
  }

/* O Widget ListView possui um atributo builder que é um callback. Este caalback lè a lista
  de dados e preenche o widget responsável pela apresentacao de dados.
*/
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: listaParceiros.length,
      itemBuilder: (context, index) {
        return ParceiroCard(
          parceiro: listaParceiros[index]['parceiro'] ?? 'Parceiro Especial',
          endereco: listaParceiros[index]['endereco'] ?? 'Não informado',
          cidade: listaParceiros[index]['cidade'] ?? 'Não informado',
          telefone: listaParceiros[index]['telefone'] ?? 'Não indormado',
        );
      },
    );
  }
}

```

#### Criar o Widget ParceiroCard
Este será o widget que irá exibir os dados dos Parceiros na ListView.

1. Crie o arquivo `widgets\parceiro_card.dart`.
2. Adicione o código abaixo:

```javascript
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ParceiroCard extends StatelessWidget {
  final double tamanhoIcone;
  final double tamanhoFonte;
  final String parceiro;
  final String endereco;
  final String cidade;
  final String telefone;

  const ParceiroCard({Key key, this.tamanhoIcone, this.tamanhoFonte, this.parceiro, this.endereco, this.cidade, this.telefone});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: Row(
          children: [
            Image.asset(
              'images/pizza.jpg',
              height: 150,
              width: 150,
              fit: BoxFit.fitHeight,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                  child: Text(
                    parceiro,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
                      child: Icon(
                        FontAwesomeIcons.mapMarker,
                        size: 20,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                    SizedBox(
                      width: 190,
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text(
                          endereco,
                          style: TextStyle(fontSize: 12),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
                      child: Icon(
                        FontAwesomeIcons.globe,
                        size: 20,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                    SizedBox(
                      width: 190,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          cidade,
                          style: TextStyle(fontSize: 12),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
                      child: Icon(
                        FontAwesomeIcons.mobile,
                        size: 20,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                    SizedBox(
                      width: 190,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          telefone,
                          style: TextStyle(fontSize: 12),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

```

Deixei uma imagem de pizza como padrão para os tres Parceiros da Lista, voce podem trocar as imagens; lembrem-se de adicionar a referencia a elas no `pubspec.yaml` e criar o diretorio images no mesmo nível de `main.dart` conforme o exemplo abaixo.

```javascript
# To add assets to your application, add an assets section, like this:
assets:
  - images/logo.png
  - images/pizza.jpg

```
### Trick de persistência

Nós iremos armazenar o arquivo em JSON com os dados de 3 parceiros. Precisamos fazer com que o arquivo seja salvo pelo mnos uma vez, então nós poderíamos associar uma função a um botão ou a o método initState, é o que iremos fazer, mas vocês precisam seguir a risca este procedimento.

1. Configure a imagem que voce quer que apareça no card conforme a instrução acima;
2. Adicione ao `servicos.dart` o método de salvar arquivo no initState.

```javascript
@override
void initState() {
  FileUtils.salvarArquivo();
  FileUtils.lerArquivo().then((value) {
    setState(() {
      listaParceiros = jsonDecode(value);
    });
  });
  super.initState();
}

```
3. Inicie o aplicativo;
4. Remova os dados do arquivo `parceiro_data.dart`.

```javascript
class ParceiroData {
  static var listaParceiros = [];
}

/*
Parceiro(
      parceiro: 'Nostra Pizza',
      imagem: 'pizza.jpg',
      endereco: 'Rua Araraquara, 500 - Jd S Joao - Araras/SP',
      telefone: '19983330406',
      url: 'https://www.facebook.com/nostrapizza',
      whatsApp: '19983339876',
      instagram: '@nostrapizza',
      horario: 'Seg-Dom: 06:00 as 21:00',
    ).toMap(),
    Parceiro(
      parceiro: 'Scudetto',
      imagem: 'pizza.jpg',
      endereco: 'Rua Tiradentes, 200 - Centro - Araras/SP',
      telefone: '19983330406',
      url: 'https://www.facebook.com/scudetto',
      whatsApp: '19983332345',
      instagram: '@scudetto',
      horario: 'Seg-Dom: 06:00 as 21:00',
    ).toMap(),
    Parceiro(
      parceiro: 'VegPizza',
      imagem: 'pizza.jpg',
      endereco: 'Rua Salamandra, 400 - Jd Green Ville - Limeira/SP',
      telefone: '19983338877',
      url: 'https://www.facebook.com/vegpizza',
      whatsApp: '19983336539',
      instagram: '@vegpizza',
      horario: 'Seg-Dom: 06:00 as 21:00',
    ).toMap(),
*/

```

5. Remova o método salvar do initState em `servicos.dart`.

```javascript
@override
void initState() {
  FileUtils.lerArquivo().then((value) {
    setState(() {
      listaParceiros = jsonDecode(value);
    });
  });
  super.initState();
}
```

Reinicie o App.
